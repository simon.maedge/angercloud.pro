cat html/header.html > index.html
cat html/body_start.html >> index.html
echo "<h3>Stand: $(date +%d.%m.%Y)</h3>" >> index.html

for pdf in pdf/*pdf; do
	echo "<li><a href=\"$pdf\">$(basename "$pdf" .pdf)</a></li>" >> index.html
done
echo "<li><a href=pdf/scanned/>Handschriftliche Dokumente (alt)</a></li>" >> index.html

cat html/body_end.html >> index.html
